CREATE TABLE IpaRole
(
	roleId serial  primary key ,
	roleName varchar(100)  NOT NULL default'' ,
	description varchar(200)  NOT NULL default''
);

CREATE UNIQUE  INDEX AK_Role ON IpaRole
(
	roleName  ASC
);


CREATE TABLE IpaUser
(
	userId serial PRIMARY key,
	surName varchar(100)  NOT NULL default '' ,
	firstName varchar(100)  NOT NULL default '',
	lastName varchar(100)  NOT NULL default '',
	email varchar(100)  NOT NULL default '',
	createdDate timestamp  NOT NULL default CURRENT_TIMESTAMP,
	createdBy varchar(100)  NOT NULL default '',
	lastModifiedBy varchar(100)  NOT NULL default '',
	lastModifiedDate timestamp  NOT NULL default CURRENT_TIMESTAMP ,
	pass bytea  NULL ,
	userSalt varchar(100)  NOT NULL default '',
	login varchar(100)  NOT NULL 
);
CREATE UNIQUE INDEX AK_User ON IpaUser
(
	login  ASC
);


CREATE TABLE IpaUserRole
(
	idUserRole serial PRIMARY key,
	userId integer  NOT NULL,
	roleId integer  NOT NULL 
);

CREATE UNIQUE INDEX AK_IpaUserRole ON IpaUserRole
(
	userId  ASC,
	roleId  ASC
);

ALTER TABLE IpaUserRole
	ADD CONSTRAINT  Fk_IpaUserRole_IpaUser FOREIGN KEY (userId) REFERENCES IpaUser(userId)
		ON DELETE cascade
		ON UPDATE cascade;

ALTER TABLE IpaUserRole
	ADD CONSTRAINT  Fk_IpaUserRole_IpaRole FOREIGN KEY (roleId) REFERENCES IpaRole(roleId)
		ON DELETE cascade
		ON UPDATE cascade;

-----------------------------------------
