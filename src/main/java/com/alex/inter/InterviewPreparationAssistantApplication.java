package com.alex.inter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InterviewPreparationAssistantApplication {

	public static void main(String[] args) {
		SpringApplication.run(InterviewPreparationAssistantApplication.class, args);
	}

}
