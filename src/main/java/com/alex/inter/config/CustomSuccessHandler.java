package com.alex.inter.config;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

import com.alex.inter.dom.IpaUser;

public class CustomSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

	private static final Logger log = LogManager.getLogger(CustomSuccessHandler.class);
	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {

		// для 
		String remoteAddr = "";
		if (request != null) {
			remoteAddr = request.getHeader("X-FORWARDED-FOR");
			if (remoteAddr == null || "".equals(remoteAddr)) {
				remoteAddr = request.getRemoteAddr();
			}
		}

		IpaUser user = ((IpaUser) authentication.getPrincipal());
		user.setIp(remoteAddr);

		String targetUrl = super.determineTargetUrl(request, response);
		if (StringUtils.isBlank(targetUrl) || StringUtils.equals(targetUrl, "/")) {
			targetUrl = "/home";
		}
		clearAuthenticationAttributes(request);
		log.info("Инициализация пользователя : " + user.getUsername() + " - " + remoteAddr);
		redirectStrategy.sendRedirect(request, response, targetUrl);
	}
}