package com.alex.inter.config;

import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.alex.inter.dom.IpaUser;
import com.alex.inter.security.IpaUserSecurity;
import com.alex.inter.service.UserService;

/**
 * @author Alex
 * 
 *   Класс для получения пользовательских данных из БД 
 *   и проверке пароля.
 *   
 *   Заменяем своей реализацией один из стандартных провайдеров Spring
 */
@Component
public class IpaAuthencationProvider implements AuthenticationProvider {

	@Autowired
	UserService userService;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String userName = authentication.getName();
		String password = authentication.getCredentials().toString();
		// получаем пользователя
		UserDetails ipaUser = userService.loadUserByUsername(userName);
		if (ipaUser != null) {
			IpaUserSecurity ipaSec = new IpaUserSecurity();
			String uSalt = ((IpaUser) ipaUser).getUserSalt();
			// временно для отладки
			if (!userName.equals("admin")) {
				byte[] newPass = ipaSec.getPassHash(password, uSalt);
				if (!DatatypeConverter.printHexBinary(newPass)
						.equals(DatatypeConverter.printHexBinary(((IpaUser) ipaUser).getPass()))) {
					throw new BadCredentialsException("Неправильный пароль");
				}
			}
			IpaUser user = (IpaUser) ipaUser;
			user.setPassword(password);
			user.setUsername(userName);
		}
		Authentication auth = new UsernamePasswordAuthenticationToken(ipaUser, password, ipaUser.getAuthorities());
		return auth;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

}
