package com.alex.inter.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.thymeleaf.extras.springsecurity5.dialect.SpringSecurityDialect;

/**
 * @author Alex 
 *
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig {

	// Внедряем свой провайдер аутентификации
	@Autowired
	IpaAuthencationProvider authProvider;

	@Bean
	public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
		http.csrf().disable().authorizeRequests()
				// Доступ только для пользователей с ролью Администратор
				.antMatchers("/admin/**").hasRole("ADMIN")
				// Доступ разрешен всем пользователям
				.antMatchers("/", "/webjars/**", "/css/**", "/img/**").permitAll()
				// Все остальные страницы требуют аутентификации
				.anyRequest().authenticated().and()
				// Настройка для входа в систему 
				.formLogin().loginPage("/login").failureUrl("/?error")
//		.defaultSuccessUrl("/home").and().logout().permitAll().logoutSuccessUrl("/");
		// Можно было обойтись по умолчанию, но для примера настроим обработку успешного входа		
		.successHandler(successHandler()).and().logout().permitAll().logoutSuccessUrl("/");
//		.and().exceptionHandling().accessDeniedHandler(deniedHandler());
		return http.build();
	}
 
	
	@Bean
	public CustomSuccessHandler successHandler() {
		return new CustomSuccessHandler();
	}
//
//	@Bean
//	public CustomAccesssDeniedHandler deniedHandler() {
//		return new CustomAccesssDeniedHandler();
//	}

	// для отображения тегов в html
	@Bean
	public SpringSecurityDialect securityDialect() {
		return new SpringSecurityDialect();
	}

	// устанавливаем свой провайдер
	@Autowired
	protected void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(authProvider);
	}
}