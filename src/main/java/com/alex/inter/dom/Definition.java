package com.alex.inter.dom;

import javax.validation.constraints.NotBlank;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Document("Definition")
public class Definition {
	private @NonNull String id;
	@NotBlank
	private @NonNull String term;
	@NotBlank
	private @NonNull String definition;
	@NotBlank
	private @NonNull String source;
}
