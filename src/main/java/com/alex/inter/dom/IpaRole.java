package com.alex.inter.dom;

import org.springframework.security.core.GrantedAuthority;

public class IpaRole implements GrantedAuthority {

	private int roleId;
	private String roleName;
	private String description;

	@Override
	public String getAuthority() {
		return "ROLE_" + roleName.toUpperCase();
	}

}
