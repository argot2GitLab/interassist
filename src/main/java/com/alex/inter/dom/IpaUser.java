package com.alex.inter.dom;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;

import javax.validation.constraints.NotBlank;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

/**
 * @author Alex
 *
 */
@Getter
@Setter
@NoArgsConstructor
public class IpaUser implements UserDetails {

	private static final long serialVersionUID = 1L;

	private @NonNull String login;
	private byte[] pass;
	private @NonNull String userSalt;
	private int userId;
	private @NonNull String surName;
	private @NonNull String firstName;
	private @NonNull String lastName;
	private @NonNull String email;

	private ArrayList<IpaRole> roles = new ArrayList<IpaRole>();

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy HH:mm")
	private LocalDateTime lastModifiedDate;
	private LocalDateTime createdDate;
	private @NonNull String lastModifiedBy;
	private @NonNull String createdBy;

	private @NonNull String ip;
	// Поля для Spring Security
	private @NonNull String password;
	private @NonNull String username;

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return roles;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public boolean isAccountNonExpired() {
		// Если true то срок действия аакунта не истёк(действителен)
		return true;
	}

	/**
	 * Указывает заблокирован или нет пользователь заблокированный пользователь не
	 * может быти аутентифицирован
	 */
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	/**
	 * true если учётные данные пользователя действительны
	 */
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	/**
	 * true если пользователь включён
	 */
	@Override
	public boolean isEnabled() {
		return true;
	}

	public String getFio() {
		return surName + " " + (firstName.trim().equals("") ? "" : firstName.trim().substring(0, 1)) + "."
				+ (lastName.trim().equals("") ? "" : lastName.trim().substring(0, 1)) + ".";
	}

	public String getAppSign() {
		return login + "  ip: " + ip;
	}
}
