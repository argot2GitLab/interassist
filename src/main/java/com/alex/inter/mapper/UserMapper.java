package com.alex.inter.mapper;

import java.util.ArrayList;

import com.alex.inter.dom.IpaRole;
import com.alex.inter.dom.IpaUser;

public interface UserMapper {

	public ArrayList<IpaUser> getAllUsers();

	public IpaUser getAppUser(String userName);

	public IpaUser getUserById(int usId);

	public void insertUser(IpaUser us);

	public void updateUser(IpaUser us);

	// Чтобы не разводить множество файлов мапперов
	// объединим роли и пользователи в один
	public ArrayList<IpaRole> getAllRoles();

	public ArrayList<IpaRole> getAllUserRoles(int userId);

	public IpaRole getRoleById(int roleId);

}
