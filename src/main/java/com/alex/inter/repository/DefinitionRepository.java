package com.alex.inter.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.alex.inter.dom.Definition;

public interface DefinitionRepository extends MongoRepository<Definition, String> {
  
    
    @Query("{id :?0}")                                          
    Optional<Definition> getDefinitionById(Integer id);

    @Query("{term : ?0}")                                     
    List<Definition> getDefinitionByTerm(String term);

}