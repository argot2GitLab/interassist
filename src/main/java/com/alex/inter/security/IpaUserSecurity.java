package com.alex.inter.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.xml.bind.DatatypeConverter;

public class IpaUserSecurity {

	public IpaUserSecurity() {
	}

	public byte[] getPassHash(CharSequence pass) {
		byte[] code;
		try {
			MessageDigest digester = MessageDigest.getInstance("SHA-512");
			byte[] input = pass.toString().getBytes();
			code = digester.digest(input);
		} catch (NoSuchAlgorithmException e) {
			throw new IllegalStateException(e);
		}
		return code;
	}

	public byte[] getPassHash(CharSequence pass, String salt) {
		byte[] digest = null;
		try {
			MessageDigest digester = MessageDigest.getInstance("SHA-512");
			byte[] slt = salt.getBytes();
			byte[] input = digester.digest(pass.toString().getBytes());
//			System.out.println(DatatypeConverter.printHexBinary(input));
			digester.update(slt);
			digest = digester.digest(input);
//			System.out.println("Соль:  " + DatatypeConverter.printHexBinary(slt));
//			System.out.println("С солью:  " + DatatypeConverter.printHexBinary(digest));
		} catch (NoSuchAlgorithmException e) {
			throw new IllegalStateException(e);
		}
		return digest;
	}

	public String getNewUserSalt() {
		String salt = "";
		try {
			byte[] slt = new byte[16];
			SecureRandom.getInstanceStrong().nextBytes(slt);
			salt = DatatypeConverter.printHexBinary(slt);
		} catch (NoSuchAlgorithmException e) {
			throw new IllegalStateException(e);
		}
		return salt;
	}

	public String generateUserPass() {
		String pass = "";
		try {
			byte[] psb = new byte[4];
			SecureRandom.getInstanceStrong().nextBytes(psb);
			pass = DatatypeConverter.printHexBinary(psb);
		} catch (NoSuchAlgorithmException e) {
			throw new IllegalStateException(e);
		}
		return pass;
	}

}
