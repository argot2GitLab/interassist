package com.alex.inter.service;

import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alex.inter.dom.Definition;
import com.alex.inter.repository.DefinitionRepository;

@Service
public class ArticleService {

	@Autowired
	DefinitionRepository defRepo;

	public Definition getDefinitionByTerm(String term) throws Exception {
		Definition def = defRepo.getDefinitionByTerm(term).stream().findFirst().orElse(null);
		return def;
	}

	public Definition getRandomDefinition() throws Exception {
		List<Definition> list;
		list = defRepo.findAll();
		Collections.shuffle(list, new Random(System.currentTimeMillis()));
		Definition def = list.stream().findAny().orElse(null);
		return def;
	}

	public void insertDefinition(Definition def) throws Exception {
		defRepo.save(def);
	}
}
