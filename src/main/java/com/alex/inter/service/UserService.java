package com.alex.inter.service;

import java.util.ArrayList;

import javax.validation.Valid;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.alex.inter.dom.IpaRole;
import com.alex.inter.dom.IpaUser;
import com.alex.inter.mapper.UserMapper;

@Service
public class UserService implements UserDetailsService {

	@Autowired
	SqlSessionFactory sqlSessionFactory;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		try (SqlSession sess = sqlSessionFactory.openSession()) {
			UserMapper userMapper = sess.getMapper(UserMapper.class);
			IpaUser user = userMapper.getAppUser(username);
			if (user == null) {
				throw new UsernameNotFoundException("Пользователь не найден");
			} else {
				user.getRoles().addAll(userMapper.getAllUserRoles(user.getUserId()));
			}

			return user;
		}

	}

	public IpaUser findUserById(int userId) throws Exception {
		try (SqlSession sess = sqlSessionFactory.openSession()) {
			UserMapper userMapper = sess.getMapper(UserMapper.class);
			IpaUser userFromDb = userMapper.getUserById(userId);
			return userFromDb;
		}
	}

	public IpaRole findRoleById(int roleId) throws Exception {
		try (SqlSession sess = sqlSessionFactory.openSession()) {
			UserMapper userMapper = sess.getMapper(UserMapper.class);
			return userMapper.getRoleById(roleId);
		}
	}

	public ArrayList<IpaRole> getAllRoles() throws Exception {
		try (SqlSession sess = sqlSessionFactory.openSession()) {
			UserMapper roleMapper = sess.getMapper(UserMapper.class);
			return roleMapper.getAllRoles();
		}
	}

	public boolean insertUser(IpaUser user) throws Exception {
		try (SqlSession sess = sqlSessionFactory.openSession()) {
			UserMapper userMapper = sess.getMapper(UserMapper.class);
			userMapper.insertUser(user);
			return true;
		}
	}

	public void updateUser(@Valid IpaUser user) {
		try (SqlSession sess = sqlSessionFactory.openSession()) {
			UserMapper userMapper = sess.getMapper(UserMapper.class);
			userMapper.updateUser(user);
		}
	}

}