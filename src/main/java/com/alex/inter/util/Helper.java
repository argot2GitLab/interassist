package com.alex.inter.util;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import com.alex.inter.dom.IpaUser;

public class Helper {

	public static String getErrorMessageFromBindingResult(BindingResult bindingResult) {
		String mes = "";
		for (FieldError error : bindingResult.getFieldErrors()) {
			mes += (error.getObjectName() + ": поле " + error.getField() + " - " + error.getDefaultMessage()) + ";  ";
		}
		return mes;
	}

	public static IpaUser getAppUser() {
		if (SecurityContextHolder.getContext().getAuthentication() != null)
			return (IpaUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		else
			return null;
	}
}
