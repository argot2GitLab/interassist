package com.alex.inter.web.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alex.inter.dom.Definition;
import com.alex.inter.service.ArticleService;

@Controller
@RequestMapping("/")
public class HomeController {

	final Logger log = LogManager.getLogger(HomeController.class);

	@Autowired
	ArticleService artService;

	/**
	 * при обращении к адресу сайта выводим форму входа
	 * 
	 * @return название формы входа
	 */
	@GetMapping("")
	public String getLoginForm() {
		return "loginForm";
	}

	/**
	 * @param model - модель дял добавления атрибутов, отображаемых на странице html
	 * @return название html страницы для отображения
	 */
	@GetMapping("home")
	public String getMainForm(Model model) {
		Definition def = null;
		try {
			def = artService.getRandomDefinition();
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		if (def == null) {
			def = new Definition();
		}
		model.addAttribute("def", def);
		return "mainForm";
	}
}
