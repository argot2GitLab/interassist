package com.alex.inter.web.controller;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.alex.inter.dom.Definition;
import com.alex.inter.service.ArticleService;
import com.alex.inter.util.Helper;
import com.alex.inter.web.form.DivAlert;
import com.alex.inter.web.form.Message;

@Controller
@RequestMapping("/write")
public class WriteController {

	final Logger log = LogManager.getLogger(WriteController.class);

	@Autowired
	ArticleService artService;

	/**
	 * Загружает форму добавления термина
	 * 
	 * @param model - модель дял добавления атрибутов, отображаемых на странице html
	 * @return название html страницы для отображения
	 */
	@GetMapping("addTerm")
	public String getAddTermForm(Model model) {
		Definition def = new Definition();
		model.addAttribute("def", def);
		return "write/addDefinitionForm";
	}

	/**
	 * @param def
	 * @param bindingResult
	 * @param model
	 * @param redirectAttributes
	 * @return
	 */
	@PostMapping("saveDefinition")
	public String saveDefinition(@Valid @ModelAttribute("def") Definition def, BindingResult bindingResult, Model model,
			RedirectAttributes redirectAttributes) {
		if (bindingResult.hasErrors()) {
			String mes = Helper.getErrorMessageFromBindingResult(bindingResult);
			log.error(mes);
			model.addAttribute("def", def);
			model.addAttribute("message", new Message(DivAlert.DANGER, true, mes));
			return "write/addDefinitionForm";
		}
		try {
			artService.insertDefinition(def);
		} catch (Exception e) {
			log.error("Ошибка записи определения: " + e.getMessage());
			model.addAttribute("def", def);
			model.addAttribute("message", new Message(DivAlert.DANGER, true, e.getMessage()));
			return "write/addDefinitionForm";
		}
		redirectAttributes.addFlashAttribute("message",
				new Message(DivAlert.SUCCESS, true, "Определение " + def.getDefinition() + "  успешно добавлено"));
		model.asMap().clear();
		return "redirect:/write/addTerm";
	}

}
