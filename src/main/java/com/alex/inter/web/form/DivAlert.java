package com.alex.inter.web.form;

public enum DivAlert {
	DANGER("alert alert-danger"), SUCCESS("alert alert-success"), WARNING("alert alert-warning");

	private final String alertClass;

	private DivAlert(String ac) {
		this.alertClass = ac;
	}

	public String getAlertClass() {
		return alertClass;
	}

}
