package com.alex.inter.web.form;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Alex
 *
 */
@Getter
@Setter
public class Message {

	private String type;

	private String message;

	public Message() {
	}

	public Message(DivAlert al, boolean isClosed, String message) {
		this.type = al.getAlertClass();
		this.message = message;
		if (isClosed) {
			type += " alert-dismissible fade show";
		}

	}
}
