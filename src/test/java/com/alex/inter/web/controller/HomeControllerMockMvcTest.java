package com.alex.inter.web.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest // Для интеграционного теста
//@WebMvcTest // для модульного теста
@AutoConfigureMockMvc // Эта аннотация нужна для того, чтобы появилась возможность внедрить в тестовый
						// класс бин MockMvc
//@WithMockUser(username = "admin", authorities = {"ADMIN"})
//имитация  пользователя 
@WithUserDetails("admin")
public class HomeControllerMockMvcTest {

	@Autowired
	private MockMvc mockMvc;

	// Этот класс преобразовывает объект в JSON-строку. Он нужен, так как мы
	// тестируем REST API, MockMvc самостоятельно это преобразование не делает.
	@Autowired
	private ObjectMapper objectMapper;

	@Test
	public void getSdmListTest() throws Exception {
		this.mockMvc.perform(get("/home")).andDo(print()).andExpect(status().isOk());
	}
}
